#!/bin/sh

# [Set ID PIN to be LOW -> we are HOST now]
cd /sys/class/gpio/
echo 50 > export
cd gpio50
echo out > direction
echo 0 > value

# [Disable USB autosuspend]
echo -1 > /sys/bus/usb/devices/usb1/power/autosuspend

exit 0
