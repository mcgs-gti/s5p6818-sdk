#!/bin/sh

#START_URL=http://127.0.0.1

env DISPLAY=:0 /usr/lib/chromium/chromium-bin ${START_URL}

# Other handy parameters:
# --no-first-run
# --kiosk
# --enable-smooth-scrolling
exit 0
