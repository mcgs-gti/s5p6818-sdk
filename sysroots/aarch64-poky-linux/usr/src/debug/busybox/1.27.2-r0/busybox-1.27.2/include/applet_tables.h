/* This is a generated file, don't edit */

#define NUM_APPLETS 175
#define KNOWN_APPNAME_OFFSETS 8

const uint16_t applet_nameofs[] ALIGN2 = {
118,
258,
391,
534,
674,
816,
960,
};

const char applet_names[] ALIGN1 = ""
"[" "\0"
"[[" "\0"
"addgroup" "\0"
"adduser" "\0"
"ash" "\0"
"awk" "\0"
"basename" "\0"
"beep" "\0"
"blkid" "\0"
"bunzip2" "\0"
"bzcat" "\0"
"cat" "\0"
"chattr" "\0"
"chgrp" "\0"
"chmod" "\0"
"chown" "\0"
"chroot" "\0"
"chvt" "\0"
"clear" "\0"
"cmp" "\0"
"cp" "\0"
"cpio" "\0"
"cut" "\0"
"date" "\0"
"dc" "\0"
"dd" "\0"
"deallocvt" "\0"
"delgroup" "\0"
"deluser" "\0"
"depmod" "\0"
"devmem" "\0"
"df" "\0"
"diff" "\0"
"dirname" "\0"
"dmesg" "\0"
"dnsdomainname" "\0"
"du" "\0"
"dumpkmap" "\0"
"dumpleases" "\0"
"echo" "\0"
"egrep" "\0"
"env" "\0"
"expr" "\0"
"false" "\0"
"fbset" "\0"
"fdisk" "\0"
"fgrep" "\0"
"find" "\0"
"flock" "\0"
"free" "\0"
"fsck" "\0"
"fstrim" "\0"
"fuser" "\0"
"getopt" "\0"
"getty" "\0"
"grep" "\0"
"groups" "\0"
"gunzip" "\0"
"gzip" "\0"
"halt" "\0"
"head" "\0"
"hexdump" "\0"
"hostname" "\0"
"hwclock" "\0"
"id" "\0"
"ifconfig" "\0"
"ifdown" "\0"
"ifup" "\0"
"insmod" "\0"
"ip" "\0"
"kill" "\0"
"killall" "\0"
"klogd" "\0"
"less" "\0"
"ln" "\0"
"loadfont" "\0"
"loadkmap" "\0"
"logger" "\0"
"logname" "\0"
"logread" "\0"
"losetup" "\0"
"ls" "\0"
"lsmod" "\0"
"lzcat" "\0"
"md5sum" "\0"
"mesg" "\0"
"microcom" "\0"
"mkdir" "\0"
"mkfifo" "\0"
"mknod" "\0"
"mkswap" "\0"
"mktemp" "\0"
"modprobe" "\0"
"more" "\0"
"mount" "\0"
"mv" "\0"
"nc" "\0"
"netstat" "\0"
"nohup" "\0"
"nproc" "\0"
"nslookup" "\0"
"od" "\0"
"openvt" "\0"
"patch" "\0"
"pidof" "\0"
"pivot_root" "\0"
"poweroff" "\0"
"printf" "\0"
"ps" "\0"
"pwd" "\0"
"rdate" "\0"
"readlink" "\0"
"realpath" "\0"
"reboot" "\0"
"renice" "\0"
"reset" "\0"
"resize" "\0"
"rm" "\0"
"rmdir" "\0"
"rmmod" "\0"
"route" "\0"
"run-parts" "\0"
"sed" "\0"
"seq" "\0"
"setconsole" "\0"
"sh" "\0"
"sha1sum" "\0"
"sha256sum" "\0"
"shuf" "\0"
"sleep" "\0"
"sort" "\0"
"start-stop-daemon" "\0"
"stat" "\0"
"strings" "\0"
"stty" "\0"
"sulogin" "\0"
"swapoff" "\0"
"swapon" "\0"
"switch_root" "\0"
"sync" "\0"
"sysctl" "\0"
"syslogd" "\0"
"tail" "\0"
"tar" "\0"
"tee" "\0"
"telnet" "\0"
"test" "\0"
"tftp" "\0"
"time" "\0"
"top" "\0"
"touch" "\0"
"tr" "\0"
"true" "\0"
"tty" "\0"
"udhcpc" "\0"
"udhcpd" "\0"
"umount" "\0"
"uname" "\0"
"uniq" "\0"
"unlink" "\0"
"unzip" "\0"
"uptime" "\0"
"users" "\0"
"usleep" "\0"
"vi" "\0"
"watch" "\0"
"wc" "\0"
"wget" "\0"
"which" "\0"
"who" "\0"
"whoami" "\0"
"xargs" "\0"
"xzcat" "\0"
"yes" "\0"
"zcat" "\0"
;

#define APPLET_NO_addgroup 2
#define APPLET_NO_adduser 3
#define APPLET_NO_ash 4
#define APPLET_NO_awk 5
#define APPLET_NO_basename 6
#define APPLET_NO_beep 7
#define APPLET_NO_blkid 8
#define APPLET_NO_bunzip2 9
#define APPLET_NO_bzcat 10
#define APPLET_NO_cat 11
#define APPLET_NO_chattr 12
#define APPLET_NO_chgrp 13
#define APPLET_NO_chmod 14
#define APPLET_NO_chown 15
#define APPLET_NO_chroot 16
#define APPLET_NO_chvt 17
#define APPLET_NO_clear 18
#define APPLET_NO_cmp 19
#define APPLET_NO_cp 20
#define APPLET_NO_cpio 21
#define APPLET_NO_cut 22
#define APPLET_NO_date 23
#define APPLET_NO_dc 24
#define APPLET_NO_dd 25
#define APPLET_NO_deallocvt 26
#define APPLET_NO_delgroup 27
#define APPLET_NO_deluser 28
#define APPLET_NO_depmod 29
#define APPLET_NO_devmem 30
#define APPLET_NO_df 31
#define APPLET_NO_diff 32
#define APPLET_NO_dirname 33
#define APPLET_NO_dmesg 34
#define APPLET_NO_dnsdomainname 35
#define APPLET_NO_du 36
#define APPLET_NO_dumpkmap 37
#define APPLET_NO_dumpleases 38
#define APPLET_NO_echo 39
#define APPLET_NO_egrep 40
#define APPLET_NO_env 41
#define APPLET_NO_expr 42
#define APPLET_NO_false 43
#define APPLET_NO_fbset 44
#define APPLET_NO_fdisk 45
#define APPLET_NO_fgrep 46
#define APPLET_NO_find 47
#define APPLET_NO_flock 48
#define APPLET_NO_free 49
#define APPLET_NO_fsck 50
#define APPLET_NO_fstrim 51
#define APPLET_NO_fuser 52
#define APPLET_NO_getopt 53
#define APPLET_NO_getty 54
#define APPLET_NO_grep 55
#define APPLET_NO_groups 56
#define APPLET_NO_gunzip 57
#define APPLET_NO_gzip 58
#define APPLET_NO_halt 59
#define APPLET_NO_head 60
#define APPLET_NO_hexdump 61
#define APPLET_NO_hostname 62
#define APPLET_NO_hwclock 63
#define APPLET_NO_id 64
#define APPLET_NO_ifconfig 65
#define APPLET_NO_ifdown 66
#define APPLET_NO_ifup 67
#define APPLET_NO_insmod 68
#define APPLET_NO_ip 69
#define APPLET_NO_kill 70
#define APPLET_NO_killall 71
#define APPLET_NO_klogd 72
#define APPLET_NO_less 73
#define APPLET_NO_ln 74
#define APPLET_NO_loadfont 75
#define APPLET_NO_loadkmap 76
#define APPLET_NO_logger 77
#define APPLET_NO_logname 78
#define APPLET_NO_logread 79
#define APPLET_NO_losetup 80
#define APPLET_NO_ls 81
#define APPLET_NO_lsmod 82
#define APPLET_NO_lzcat 83
#define APPLET_NO_md5sum 84
#define APPLET_NO_mesg 85
#define APPLET_NO_microcom 86
#define APPLET_NO_mkdir 87
#define APPLET_NO_mkfifo 88
#define APPLET_NO_mknod 89
#define APPLET_NO_mkswap 90
#define APPLET_NO_mktemp 91
#define APPLET_NO_modprobe 92
#define APPLET_NO_more 93
#define APPLET_NO_mount 94
#define APPLET_NO_mv 95
#define APPLET_NO_nc 96
#define APPLET_NO_netstat 97
#define APPLET_NO_nohup 98
#define APPLET_NO_nproc 99
#define APPLET_NO_nslookup 100
#define APPLET_NO_od 101
#define APPLET_NO_openvt 102
#define APPLET_NO_patch 103
#define APPLET_NO_pidof 104
#define APPLET_NO_pivot_root 105
#define APPLET_NO_poweroff 106
#define APPLET_NO_printf 107
#define APPLET_NO_ps 108
#define APPLET_NO_pwd 109
#define APPLET_NO_rdate 110
#define APPLET_NO_readlink 111
#define APPLET_NO_realpath 112
#define APPLET_NO_reboot 113
#define APPLET_NO_renice 114
#define APPLET_NO_reset 115
#define APPLET_NO_resize 116
#define APPLET_NO_rm 117
#define APPLET_NO_rmdir 118
#define APPLET_NO_rmmod 119
#define APPLET_NO_route 120
#define APPLET_NO_sed 122
#define APPLET_NO_seq 123
#define APPLET_NO_setconsole 124
#define APPLET_NO_sh 125
#define APPLET_NO_sha1sum 126
#define APPLET_NO_sha256sum 127
#define APPLET_NO_shuf 128
#define APPLET_NO_sleep 129
#define APPLET_NO_sort 130
#define APPLET_NO_stat 132
#define APPLET_NO_strings 133
#define APPLET_NO_stty 134
#define APPLET_NO_sulogin 135
#define APPLET_NO_swapoff 136
#define APPLET_NO_swapon 137
#define APPLET_NO_switch_root 138
#define APPLET_NO_sync 139
#define APPLET_NO_sysctl 140
#define APPLET_NO_syslogd 141
#define APPLET_NO_tail 142
#define APPLET_NO_tar 143
#define APPLET_NO_tee 144
#define APPLET_NO_telnet 145
#define APPLET_NO_test 146
#define APPLET_NO_tftp 147
#define APPLET_NO_time 148
#define APPLET_NO_top 149
#define APPLET_NO_touch 150
#define APPLET_NO_tr 151
#define APPLET_NO_true 152
#define APPLET_NO_tty 153
#define APPLET_NO_udhcpc 154
#define APPLET_NO_udhcpd 155
#define APPLET_NO_umount 156
#define APPLET_NO_uname 157
#define APPLET_NO_uniq 158
#define APPLET_NO_unlink 159
#define APPLET_NO_unzip 160
#define APPLET_NO_uptime 161
#define APPLET_NO_users 162
#define APPLET_NO_usleep 163
#define APPLET_NO_vi 164
#define APPLET_NO_watch 165
#define APPLET_NO_wc 166
#define APPLET_NO_wget 167
#define APPLET_NO_which 168
#define APPLET_NO_who 169
#define APPLET_NO_whoami 170
#define APPLET_NO_xargs 171
#define APPLET_NO_xzcat 172
#define APPLET_NO_yes 173
#define APPLET_NO_zcat 174

#ifndef SKIP_applet_main
int (*const applet_main[])(int argc, char **argv) = {
test_main,
test_main,
addgroup_main,
adduser_main,
ash_main,
awk_main,
basename_main,
beep_main,
blkid_main,
bunzip2_main,
bunzip2_main,
cat_main,
chattr_main,
chgrp_main,
chmod_main,
chown_main,
chroot_main,
chvt_main,
clear_main,
cmp_main,
cp_main,
cpio_main,
cut_main,
date_main,
dc_main,
dd_main,
deallocvt_main,
deluser_main,
deluser_main,
depmod_main,
devmem_main,
df_main,
diff_main,
dirname_main,
dmesg_main,
hostname_main,
du_main,
dumpkmap_main,
dumpleases_main,
echo_main,
grep_main,
env_main,
expr_main,
false_main,
fbset_main,
fdisk_main,
grep_main,
find_main,
flock_main,
free_main,
fsck_main,
fstrim_main,
fuser_main,
getopt_main,
getty_main,
grep_main,
id_main,
gunzip_main,
gzip_main,
halt_main,
head_main,
hexdump_main,
hostname_main,
hwclock_main,
id_main,
ifconfig_main,
ifupdown_main,
ifupdown_main,
insmod_main,
ip_main,
kill_main,
kill_main,
klogd_main,
less_main,
ln_main,
loadfont_main,
loadkmap_main,
logger_main,
logname_main,
logread_main,
losetup_main,
ls_main,
lsmod_main,
unlzma_main,
md5_sha1_sum_main,
mesg_main,
microcom_main,
mkdir_main,
mkfifo_main,
mknod_main,
mkswap_main,
mktemp_main,
modprobe_main,
more_main,
mount_main,
mv_main,
nc_main,
netstat_main,
nohup_main,
nproc_main,
nslookup_main,
od_main,
openvt_main,
patch_main,
pidof_main,
pivot_root_main,
halt_main,
printf_main,
ps_main,
pwd_main,
rdate_main,
readlink_main,
realpath_main,
halt_main,
renice_main,
reset_main,
resize_main,
rm_main,
rmdir_main,
rmmod_main,
route_main,
run_parts_main,
sed_main,
seq_main,
setconsole_main,
ash_main,
md5_sha1_sum_main,
md5_sha1_sum_main,
shuf_main,
sleep_main,
sort_main,
start_stop_daemon_main,
stat_main,
strings_main,
stty_main,
sulogin_main,
swap_on_off_main,
swap_on_off_main,
switch_root_main,
sync_main,
sysctl_main,
syslogd_main,
tail_main,
tar_main,
tee_main,
telnet_main,
test_main,
tftp_main,
time_main,
top_main,
touch_main,
tr_main,
true_main,
tty_main,
udhcpc_main,
udhcpd_main,
umount_main,
uname_main,
uniq_main,
unlink_main,
unzip_main,
uptime_main,
who_main,
usleep_main,
vi_main,
watch_main,
wc_main,
wget_main,
which_main,
who_main,
whoami_main,
xargs_main,
unxz_main,
yes_main,
gunzip_main,
};
#endif

const uint8_t applet_suid[] ALIGN1 = {
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
};

