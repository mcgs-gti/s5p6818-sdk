/* Generated automatically by the program 'build/genpreds'
   from the machine description file '../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/aarch64.md'.  */

#ifndef GCC_TM_CONSTRS_H
#define GCC_TM_CONSTRS_H

static inline bool
satisfies_constraint_m (rtx op)
{
  return (GET_CODE (op) == MEM) && (
#line 26 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(memory_address_addr_space_p (GET_MODE (op), XEXP (op, 0),
						 MEM_ADDR_SPACE (op))));
}
static inline bool
satisfies_constraint_o (rtx op)
{
  return (GET_CODE (op) == MEM) && (
#line 32 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(offsettable_nonstrict_memref_p (op)));
}
static inline bool
satisfies_constraint_V (rtx op)
{
  return (GET_CODE (op) == MEM) && ((
#line 41 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(memory_address_addr_space_p (GET_MODE (op), XEXP (op, 0),
						 MEM_ADDR_SPACE (op)))) && (!(
#line 43 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(offsettable_nonstrict_memref_p (op)))));
}
static inline bool
satisfies_constraint__l (rtx op)
{
  return (GET_CODE (op) == MEM) && ((
#line 50 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(GET_CODE (XEXP (op, 0)) == PRE_DEC)) || (
#line 51 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(GET_CODE (XEXP (op, 0)) == POST_DEC)));
}
static inline bool
satisfies_constraint__g (rtx op)
{
  return (GET_CODE (op) == MEM) && ((
#line 57 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(GET_CODE (XEXP (op, 0)) == PRE_INC)) || (
#line 58 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(GET_CODE (XEXP (op, 0)) == POST_INC)));
}
static inline bool
satisfies_constraint_p (rtx ARG_UNUSED (op))
{
  return 
#line 62 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(address_operand (op, VOIDmode));
}
static inline bool
satisfies_constraint_i (rtx op)
{
  return (
#line 66 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(CONSTANT_P (op))) && (
#line 67 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(!flag_pic || LEGITIMATE_PIC_OPERAND_P (op)));
}
static inline bool
satisfies_constraint_s (rtx op)
{
  return (
#line 71 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(CONSTANT_P (op))) && ((
#line 72 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(!CONST_SCALAR_INT_P (op))) && (
#line 73 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(!flag_pic || LEGITIMATE_PIC_OPERAND_P (op))));
}
static inline bool
satisfies_constraint_n (rtx op)
{
  return (
#line 77 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(CONST_SCALAR_INT_P (op))) && (
#line 78 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(!flag_pic || LEGITIMATE_PIC_OPERAND_P (op)));
}
static inline bool
satisfies_constraint_E (rtx op)
{
  return (
#line 82 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(CONST_DOUBLE_AS_FLOAT_P (op))) || (
#line 83 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(GET_CODE (op) == CONST_VECTOR
		    && GET_MODE_CLASS (GET_MODE (op)) == MODE_VECTOR_FLOAT));
}
static inline bool
satisfies_constraint_F (rtx op)
{
  return (
#line 89 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(CONST_DOUBLE_AS_FLOAT_P (op))) || (
#line 90 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(GET_CODE (op) == CONST_VECTOR
		    && GET_MODE_CLASS (GET_MODE (op)) == MODE_VECTOR_FLOAT));
}
static inline bool
satisfies_constraint_X (rtx ARG_UNUSED (op))
{
  return 
#line 95 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/common.md"
(true);
}
static inline bool
satisfies_constraint_I (rtx op)
{
  HOST_WIDE_INT ival = 0;
  if (CONST_INT_P (op))
    ival = INTVAL (op);
  return (GET_CODE (op) == CONST_INT) && (
#line 36 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_uimm12_shift (ival)));
}
static inline bool
satisfies_constraint_Upl (rtx op)
{
  return (GET_CODE (op) == CONST_INT) && (
#line 41 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_pluslong_strict_immedate (op, VOIDmode)));
}
static inline bool
satisfies_constraint_J (rtx op)
{
  HOST_WIDE_INT ival = 0;
  if (CONST_INT_P (op))
    ival = INTVAL (op);
  return (GET_CODE (op) == CONST_INT) && (
#line 46 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_uimm12_shift (-ival)));
}
static inline bool
satisfies_constraint_K (rtx op)
{
  HOST_WIDE_INT ival = 0;
  if (CONST_INT_P (op))
    ival = INTVAL (op);
  return (GET_CODE (op) == CONST_INT) && (
#line 55 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_bitmask_imm (ival, SImode)));
}
static inline bool
satisfies_constraint_L (rtx op)
{
  HOST_WIDE_INT ival = 0;
  if (CONST_INT_P (op))
    ival = INTVAL (op);
  return (GET_CODE (op) == CONST_INT) && (
#line 60 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_bitmask_imm (ival, DImode)));
}
static inline bool
satisfies_constraint_M (rtx op)
{
  HOST_WIDE_INT ival = 0;
  if (CONST_INT_P (op))
    ival = INTVAL (op);
  return (GET_CODE (op) == CONST_INT) && (
#line 65 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_move_imm (ival, SImode)));
}
static inline bool
satisfies_constraint_N (rtx op)
{
  HOST_WIDE_INT ival = 0;
  if (CONST_INT_P (op))
    ival = INTVAL (op);
  return (GET_CODE (op) == CONST_INT) && (
#line 70 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_move_imm (ival, DImode)));
}
static inline bool
satisfies_constraint_UsO (rtx op)
{
  HOST_WIDE_INT ival = 0;
  if (CONST_INT_P (op))
    ival = INTVAL (op);
  return (GET_CODE (op) == CONST_INT) && (
#line 75 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_and_bitmask_imm (ival, SImode)));
}
static inline bool
satisfies_constraint_UsP (rtx op)
{
  HOST_WIDE_INT ival = 0;
  if (CONST_INT_P (op))
    ival = INTVAL (op);
  return (GET_CODE (op) == CONST_INT) && (
#line 80 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_and_bitmask_imm (ival, DImode)));
}
static inline bool
satisfies_constraint_S (rtx op)
{
  switch (GET_CODE (op))
    {
    case CONST:
    case SYMBOL_REF:
    case LABEL_REF:
      break;
    default:
      return false;
    }
  return 
#line 85 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_symbolic_address_p (op));
}
static inline bool
satisfies_constraint_Y (rtx op)
{
  return (GET_CODE (op) == CONST_DOUBLE) && (
#line 90 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_float_const_zero_rtx_p (op)));
}
static inline bool
satisfies_constraint_Z (rtx op)
{
  return 
#line 94 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(op == const0_rtx);
}
static inline bool
satisfies_constraint_Ush (rtx op)
{
  return (GET_CODE (op) == HIGH) && (
#line 99 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_valid_symref (XEXP (op, 0), GET_MODE (XEXP (op, 0)))));
}
static inline bool
satisfies_constraint_Uss (rtx op)
{
  HOST_WIDE_INT ival = 0;
  if (CONST_INT_P (op))
    ival = INTVAL (op);
  return (GET_CODE (op) == CONST_INT) && (
#line 105 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
((unsigned HOST_WIDE_INT) ival < 32));
}
static inline bool
satisfies_constraint_Usn (rtx op)
{
  HOST_WIDE_INT ival = 0;
  if (CONST_INT_P (op))
    ival = INTVAL (op);
  return (GET_CODE (op) == CONST_INT) && (
#line 110 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(IN_RANGE (ival, -31, 0)));
}
static inline bool
satisfies_constraint_Usd (rtx op)
{
  HOST_WIDE_INT ival = 0;
  if (CONST_INT_P (op))
    ival = INTVAL (op);
  return (GET_CODE (op) == CONST_INT) && (
#line 116 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
((unsigned HOST_WIDE_INT) ival < 64));
}
static inline bool
satisfies_constraint_Usf (rtx op)
{
  return (GET_CODE (op) == SYMBOL_REF) && (
#line 121 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(!aarch64_is_noplt_call_p (op)));
}
static inline bool
satisfies_constraint_UsM (rtx op)
{
  return 
#line 126 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(op == constm1_rtx);
}
static inline bool
satisfies_constraint_Ui1 (rtx op)
{
  return 
#line 131 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(op == const1_rtx);
}
static inline bool
satisfies_constraint_Ui3 (rtx op)
{
  HOST_WIDE_INT ival = 0;
  if (CONST_INT_P (op))
    ival = INTVAL (op);
  return (GET_CODE (op) == CONST_INT) && (
#line 137 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
((unsigned HOST_WIDE_INT) ival <= 4));
}
static inline bool
satisfies_constraint_Up3 (rtx op)
{
  HOST_WIDE_INT ival = 0;
  if (CONST_INT_P (op))
    ival = INTVAL (op);
  return (GET_CODE (op) == CONST_INT) && (
#line 143 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
((unsigned) exact_log2 (ival) <= 4));
}
static inline bool
satisfies_constraint_Q (rtx op)
{
  return (GET_CODE (op) == MEM) && (
#line 148 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(REG_P (XEXP (op, 0))));
}
static inline bool
satisfies_constraint_Ump (rtx op)
{
  return (GET_CODE (op) == MEM) && (
#line 154 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_legitimate_address_p (GET_MODE (op), XEXP (op, 0),
						  PARALLEL, 1)));
}
static inline bool
satisfies_constraint_Utv (rtx op)
{
  return (GET_CODE (op) == MEM) && (
#line 162 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_simd_mem_operand_p (op)));
}
static inline bool
satisfies_constraint_Ufc (rtx op)
{
  return (GET_CODE (op) == CONST_DOUBLE) && (
#line 168 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_float_const_representable_p (op)));
}
static inline bool
satisfies_constraint_Dn (rtx op)
{
  return (GET_CODE (op) == CONST_VECTOR) && (
#line 174 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_simd_valid_immediate (op, GET_MODE (op),
						 false, NULL)));
}
static inline bool
satisfies_constraint_Dh (rtx op)
{
  return (GET_CODE (op) == CONST_INT) && (
#line 182 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_simd_scalar_immediate_valid_for_move (op,
						 HImode)));
}
static inline bool
satisfies_constraint_Dq (rtx op)
{
  return (GET_CODE (op) == CONST_INT) && (
#line 190 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_simd_scalar_immediate_valid_for_move (op,
						 QImode)));
}
static inline bool
satisfies_constraint_Dl (rtx op)
{
  return (GET_CODE (op) == CONST_VECTOR) && (
#line 197 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_simd_shift_imm_p (op, GET_MODE (op),
						 true)));
}
static inline bool
satisfies_constraint_Dr (rtx op)
{
  return (GET_CODE (op) == CONST_VECTOR) && (
#line 204 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_simd_shift_imm_p (op, GET_MODE (op),
						 false)));
}
static inline bool
satisfies_constraint_Dz (rtx op)
{
  return (GET_CODE (op) == CONST_VECTOR) && (
#line 210 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_simd_imm_zero_p (op, GET_MODE (op))));
}
static inline bool
satisfies_constraint_Dd (rtx op)
{
  return (GET_CODE (op) == CONST_INT) && (
#line 216 "../../../../../../../work-shared/gcc-7.3.0-r0/gcc-7.3.0/gcc/config/aarch64/constraints.md"
(aarch64_simd_imm_scalar_p (op, GET_MODE (op))));
}
#endif /* tm-constrs.h */
