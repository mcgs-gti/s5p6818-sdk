
<!-- vim-markdown-toc GFM -->
* [SSH connect](#ssh-connect)
* [Install mcgs sdk for 'mcgstpc1071g'](#install-mcgs-sdk-for-mcgstpc1071g)
    * [download](#download)
    * [use](#use)
* [USB OTG(HOST/DEVICE)](#usb-otghostdevice)
    * [To switch to USB host](#to-switch-to-usb-host)
    * [To switch to USB device](#to-switch-to-usb-device)
* [Buzzer](#buzzer)
* [UARTs](#uarts)
    * [UART 0](#uart-0)
    * [UART 1](#uart-1)
    * [UART 2](#uart-2)
    * [UART 3](#uart-3)
* [Nodejs](#nodejs)

<!-- vim-markdown-toc -->

# SSH connect
- board default ip: 200.200.200.190
- ssh username: root
- ssh passwd/authorization: `No password and key`


# Install mcgs sdk for 'mcgstpc1071g'
## download
```language_bash
$ sudo git clone git@gitlab.com:mcgs-gti/s5p6818-sdk.git /opt/mcgs/s5p6818-sdk
```

## use
Each time you wish to use the SDK in a new shell session, you need to source the environment setup script e.g.
```language_bash
$ . /opt/mcgs/s5p6818-sdk/environment-setup-aarch64-poky-linux
```
# USB OTG(HOST/DEVICE)
## To switch to USB host
- Set ID PIN to be LOW -> we are HOST now
```language_bash
$ cd /sys/class/gpio/; echo 50 > export; cd gpio50
$ echo out > direction; sleep 1; echo 0 > vlaue
```

- Disable USB autosuspend
```language_bash
$ echo -1 > /sys/bus/usb/devices/usb1/power/autosuspend
```

- Remove USB dongle


## To switch to USB device
```language_bash
$ echo 1 > value; sleep 1; echo in > direction
```

# Buzzer
Type 'beep' from command line:
```language_bash
$ beep
```

# UARTs
## UART 0
Debug purpose -> solder cables to pins 25,26

## UART 1
2 pins -> simple MAX3232 -> J33 short cut pins 2 and 3

## UART 2
RS485 + GPIO_C5 as nUART2 data enbale pin -> GPIO number 69
```language_bash
$ echo 69 > /sys/class/gpio/export
$ echo out > /sys/class/gpio/gpio69/direction
$ echo 0 > /sys/class/gpio/gpio69/value
```

## UART 3
RS485 + GPIO_C6 as nUART3 data enbale pin -> GPIO number 70
```language_bash
$ echo 70 > /sys/class/gpio/export
$ echo out > /sys/class/gpio/gpio70/direction
$ echo 0 > /sys/class/gpio/gpio70/value
```

# Nodejs
- Setup network on the board
- Install the "Express" package(npm install express --save):  
    https://expressjs.com/en/starter/installing.html
- Save the hello world code into app.js  
    https://expressjs.com/en/starter/hello-world.html
- run "node app.js"
- Type 'localhost:3000' in the browser

